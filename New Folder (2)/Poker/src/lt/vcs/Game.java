package lt.vcs;

import static lt.vcs.VcsUtils.*;

/**
 * Zaidimo klase, reprezentuojanti viena zaidimo partija
 */
public class Game {

    /** pirmas zaidejas */
    private final Player p1;
    /** antras zaidejas */
    private final Player p2;

    private Player activePlayer;

    /**
     * zaidimo konstruktorius
     * @param p1 pirmas zaidejas
     * @param p2 antras zaidejas
     */
    public Game(Player p1, Player p2) {
        this.p1 = p1;
        this.p2 = p2;
        activePlayer = p1;
    }

    public void setActivePlayer(Player activePlayer) {
        this.activePlayer = activePlayer;
    }

    /**
     * startuoja zaidima/partija
     * 2 RETURN Player abjektas grazina laimetoja
     */
    public Player start() {
        int p1Bet = inInt(activePlayer.getName()+"Statykite");
        int p2Bet = inInt(nextActive().getName()+"Statykite");
         activePlayer.setCash(activePlayer.getCash()-p1Bet);
         nextActive().setCash(nextActive().getCash()-p2Bet);
        if (p1Bet<p2Bet){
            out(activePlayer.getName()+"pakelel iki " + p2Bet);
            int choise = inInt("ka darysite 0-islyginsi; 1-foldinti");
            if(choise == 1){
               nextActive().setCash(nextActive().getCash()+p1Bet + p2Bet);
                return nextActive();
            }
        }   
        int pot = p1Bet + p2Bet;
    
        
        activePlayer.setHand(new Hand (GameUtils.rollHand()));
        out (activePlayer.getName()+ " Jusu ranka:" + GameUtils.intArrayToString(activePlayer.getHand().getHandArray()));
        nextActive().setHand(new Hand (GameUtils.rollHand()));
        out (nextActive().getName()+ " Jusu ranka:" + GameUtils.intArrayToString(nextActive().getHand().getHandArray()));
        
        
        Player laimetojas = GameUtils.kasLaimejo(p1, p2);
        out("Laimejo "+laimetojas.getName()+" Sveikiname");
        int totalPot = getTotalPot(pot,laimetojas.getHand().getCombination());
        out("Laimejimo suma yra: "+ laimetojas.getCash());
                laimetojas.setCash(laimetojas.getCash()+totalPot);
        return laimetojas;  
    }
    
    private int getTotalPot (int pot, Combination combo){
        return pot + combo.getBonus();
    }
    
private Player nextActive(){
    return Main.getNextActivePayer(this);
}
    
    public Player getP1() {
        return p1;
    }

    public Player getP2() {
        return p2;
    }

    public Player getActivePlayer() {
        return activePlayer;
    }
   
    

}
