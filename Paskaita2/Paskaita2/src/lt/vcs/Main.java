/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lt.vcs;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import static lt.vcs.VcsUtils.*; 
import lt.vcs.Person;
import lt.vcs.Gender;
// *importuojam visus statinius 
/**
 *
 * @author probook
 */
public class Main {

     public static void main(String[] args) {
        paskaita4Kodas();
       //  paskaita3Sprendimas();
     }
     private static void paskaita4Kodas(){
         
         Object o = new Object();
         out("Object:"+o); 
         o = new Arklys();
         out("Arklys:"+o);
         o = new vienaragis();
         out("Vienaragis:"+o);
         Creature c =  new Arklys();
         Arklys ark = null;
         if(c instanceof Arklys){
            ark =( Arklys)c;
         }
         
         gyvenk(ark);
         out("Arklys:"+c);
         c= new vienaragis();
         out("Vienaragis:"+c);
         Gyvunas g = new Arklys();
         gyvenk(g);
         out("Arklys:"+g);
         g= new vienaragis();
         out("Vienaragis:"+g);
         gyvenk(new  Arklys());
         
         if(o instanceof Object){
             out("o yra objektas");
         }
          if(o instanceof Creature){
             out("o yra creature");
         }
           if(o instanceof Gyvunas){
             out("o yra gyvunas");
         }
     }
     
     private static void gyvenk (Gyvunas g){
         g.gyvent();
     }
     
}



