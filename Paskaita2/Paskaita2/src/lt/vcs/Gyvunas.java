/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lt.vcs;
import static lt.vcs.VcsUtils.*;
/**
 *
 * @author probook
 */
public abstract class Gyvunas implements Creature {

   public abstract void gyvent();
   
   public void gyventKaipGyvunai(){
       out("gyventKaipGyvunai");
   }
   
   @Override
   public String toString(){
       return this.getClass().getName() + " " + getWorld();     
   
   }
}
