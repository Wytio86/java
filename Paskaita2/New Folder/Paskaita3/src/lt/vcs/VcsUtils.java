/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lt.vcs;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

/**
 *
 * @author probook
 */
public class VcsUtils {
    
   // private Main pvz = new Main(null);
    
    public static void out(Object txt){
        System.out.println(laikas() + " "+ txt); // iskviecia To.String 
    }
    
    public static String intstr(String txt){
    out(txt);
    return intstr();
    }
    
    public static String intstr(){
        return newScan().next();
    }
    
    public static int inInt(String txt){
    out(txt);
    return inInt();
    }
    public static int inInt(){
        return newScan().nextInt();
    }
    
      private static Scanner newScan(){
          return new Scanner(System.in);     
    }
      private static String laikas(){
        SimpleDateFormat laik = new SimpleDateFormat("HH:mm:ss:SSS");
        return (laik.format(new Date())); 
      }
      
      
}

