
package lt.vcs;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import static lt.vcs.VcsUtils.*;

/**
 *
 * @author probook
 */
public class Main {

    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception {
       String ivs = inStr("Iveskite pilna failo kelia");
       File fil = new File(ivs);
       out ("Failas egzistuoja?" + fil.exists());
       
      
       FileInputStream fis1 = new FileInputStream(fil);
       InputStreamReader isr1 = new InputStreamReader(fis1, "UTF-8");
       BufferedReader br1 = new BufferedReader(isr1);
       String failoTurinys = readTextFile(br1);
       br1.close();
       
      
       BufferedWriter bw = createBR(fil);
       
       bw.append(failoTurinys);
       bw.newLine();
       bw.append("pirmas irasymas");
       bw.flush();
       bw.close();
       
       
       
       FileInputStream fis = new FileInputStream(fil);
       InputStreamReader isr = new InputStreamReader(fis, "UTF-8");
       BufferedReader br = new BufferedReader(isr);
//     out("Ivesto failo pirma eilute: "+br.readLine());
//     out("Ivesto failo pirma 2 eilute: "+br.readLine());
       out("Failo turinys: "+NL + readTextFile(br));
    }
   
    }   
    }
    

