
package lt.vcs;

/**
 *
 * @author probook
 */
public class Zmogus {
     int amzius = 18;
     String vardas ="Andrius";
     int alga = 350;

    public int getAmzius() {
        return amzius;
    }

    public void setAmzius(int amzius) {
        this.amzius = amzius;
    }

    public String getVardas() {
        return vardas;
    }

    public void setVardas(String vardas) {
        this.vardas = vardas;
    }

    public int getAlga() {
        return alga;
    }

    public void setAlga(int alga) {
        this.alga = alga;
    }
    
}
