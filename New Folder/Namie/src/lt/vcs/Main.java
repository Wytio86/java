
package lt.vcs;

/**
 *
 * @author probook
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
       
        Zmogus Jonas = new Zmogus();
        System.out.println("Jonas yra "+Jonas.getAmzius()+" amziaus ir uzdirba "+Jonas.getAlga()+" euru.");  
    
        Season Spring = Season.SUMMER;
        System.out.println(Spring.laikas);
        System.out.println(Spring);
        System.out.println();
        
        for (Season s : Season.values()){
           
            System.out.println(s.laikas+"-" + s.menuo+ "-" +s);
            
        }
    
    
    }
    
}
