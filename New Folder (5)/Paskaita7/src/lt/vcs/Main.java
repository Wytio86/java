/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lt.vcs;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import static lt.vcs.VcsUtils.*;
import static lt.vcs.VcsUtils.inLine;
import lt.vcs.Deimantas.*;
/**
 *
 * @author probook
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
//        String words = inLine("Iveskite zodzius atskirtus kableliu");
//        String[] wordMas = words.replaceAll(" ", "").split(",");
////        int[][] intMas = {
////            {1,2,3},
////            {4,5,6},
////            {7,8,9}
////        };
////        
////        out(""+intMas[1][2]);
//        List<String> pvz = new ArrayList();
//        pvz.add("asas");
//        
//        List<String>  listas = toSortedList(wordMas);
//        out("nesurusiuoti");
//        for (String word:listas){
//            out(word);
//        }
//        
//        
//      Collections.sort(listas);
//      Set<String> setas = new TreeSet(listas);
//      out("surusiuoti liste: ");
//        for (String word:setas){
//            out(word);
//        }
//         Map <String, List<String>> mapas = new HashMap();
//         List<String> neSu = Arrays.asList("asasas","dadada","fefefe");
//         mapas.put("pirmas",neSu);
//         mapas.put("antras",listas);
//         //out(mapas.get("pirmas"));
//         for (String word:mapas.get("pirmas")){
//            out(word);
//    }
         String eilute = inLine("Iveskite teksta");
         String isvalytiEilute = eilute.replaceAll(" ", "").replaceAll(",", "");
         char [] raides = isvalytiEilute.toCharArray();
         List<String> strRaides = charArrToString(raides);
         Map <String, Integer> mapas1 = new HashMap();
         for(String raide:strRaides){
             Integer value = mapas1.get(raide);
             if(value == null){
                 mapas1.put(raide, 1);
             }else{
                 mapas1.put(raide, value+1);
             }
             
         }
         List<Integer> values =new ArrayList(mapas1.values());
         Collections.sort(values);
         Collections.reverse(values);
         out("panaudotos reaides mazejimo tvarka, ignor maz and didz raides");
         for(Integer sk:values){
             List<String> panaudotos = new ArrayList();
             for(String key: mapas1.keySet()){
                 Integer val = mapas1.get(key);
                 if(!panaudotos.contains(key)&& sk.equals(val)){  
                 panaudotos.add(key);
                 out(key + " - "+ sk);
                 break;
                 }
             }
             
         }
         
    }
    private static List<String> charArrToString (char[] chars){
    List<String> result = new ArrayList();
    for(char charas:chars){
        result.add((""+chars).toLowerCase());
    }
    return result;
}
    

//    private static List <String> toSortedList(String[] strMas){
//        
//        List sarasas = Arrays.asList(strMas);
//        Collections.sort(sarasas);
//        return sarasas;
//    }
}
